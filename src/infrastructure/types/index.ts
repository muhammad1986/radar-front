import {IKeyValue} from "./KeyValue";
import {IPagination} from "./Pagination";

export type {IKeyValue, IPagination};
