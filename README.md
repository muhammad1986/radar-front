# EFA Architecture

> This approach is a little changed DDD approach but for frontend

## The most important rule

> a layer shouldn't be aware of any layers above it.

## Schema
![](https://michalzalecki.com/posts/elegant-frontend-architecture-ui@2x.jpg)

### Refs:

- https://michalzalecki.com/elegant-frontend-architecture/
